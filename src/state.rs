use anymap::AnyMap;
use glium::{framebuffer::SimpleFrameBuffer, glutin::event::Event, Display, Frame, Surface};

mod test_state;
pub use test_state::TestState;

use crate::assets::Assets;

pub trait GameState {
    fn input(&mut self, assets: &Assets, event: &Event<()>);
    fn update(&mut self, assets: &Assets, dt: f32) -> Option<Box<dyn GameState>>;
    fn render(&mut self, assets: &Assets, display: &Display, target: &mut SimpleFrameBuffer);
}

pub struct DummyState;

impl GameState for DummyState {
    fn update(&mut self, _assets: &Assets, _dt: f32) -> Option<Box<dyn GameState>> {
        None
    }

    fn input(&mut self, _assets: &Assets, _event: &Event<()>) {}

    fn render(&mut self, _assets: &Assets, _display: &Display, _target: &mut SimpleFrameBuffer) {}
}
