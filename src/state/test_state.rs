use glium::{
    framebuffer::SimpleFrameBuffer, implement_vertex, index::PrimitiveType, uniform,
    uniforms::Sampler, IndexBuffer, Surface, VertexBuffer,
};

use crate::assets::Assets;

use super::GameState;

pub struct TestState {
    pub time: f32,
}

impl GameState for TestState {
    fn input(&mut self, assets: &Assets, event: &glium::glutin::event::Event<()>) {}

    fn update(&mut self, assets: &Assets, dt: f32) -> Option<Box<dyn GameState>> {
        self.time += 0.0;
        None
    }

    fn render(
        &mut self,
        assets: &Assets,
        display: &glium::Display,
        target: &mut SimpleFrameBuffer,
    ) {
        // Make vertex buffer
        let vertex_buffer = {
            #[derive(Clone, Copy)]
            struct Vertex {
                position: [f32; 2],
                tex_coords: [f32; 2],
            }

            implement_vertex!(Vertex, position, tex_coords);

            let tex_coords = crate::assets::AtlasUVs {
                top_left: [0.0, 0.0],
                top_right: [1.0, 0.0],
                bottom_left: [0.0, 1.0],
                bottom_right: [1.0, 1.0],
            };
            let tex_coords = assets
                .texture_atlas
                .get_uv("backgrounds:grass-hills".to_string());

            VertexBuffer::new(
                display,
                &[
                    Vertex {
                        position: [-1.0, -1.0],
                        tex_coords: [
                            (tex_coords.top_left[0] - self.time.sin() / 8.0),
                            tex_coords.top_left[1],
                        ],
                    },
                    Vertex {
                        position: [-1.0, 1.0],
                        tex_coords: [
                            (tex_coords.bottom_left[0] - self.time.sin() / 8.0),
                            tex_coords.bottom_left[1],
                        ],
                    },
                    Vertex {
                        position: [1.0, 1.0],
                        tex_coords: [
                            (tex_coords.bottom_right[0] / 2.0 - self.time.sin() / 8.0),
                            tex_coords.bottom_right[1],
                        ],
                    },
                    Vertex {
                        position: [1.0, -1.0],
                        tex_coords: [
                            (tex_coords.top_right[0] / 2.0 - self.time.sin() / 8.0),
                            tex_coords.top_right[1],
                        ],
                    },
                ],
            )
            .unwrap()
        };

        let index_buffer =
            IndexBuffer::new(display, PrimitiveType::TriangleStrip, &[1 as u16, 2, 0, 3]).unwrap();

        let program = assets.shaders.get("basic").unwrap();

        let uniforms = uniform! {
            matrix: cgmath::conv::array3x3(
                cgmath::Matrix3::from_nonuniform_scale(1.0, 1.0 as f32)
            ),
            texture: assets.texture_atlas.texture.sampled().magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest),
        };

        target
            .draw(
                &vertex_buffer,
                &index_buffer,
                program,
                &uniforms,
                &Default::default(),
            )
            .unwrap();
    }
}
