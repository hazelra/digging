#version 330 core
precision lowp float;

in vec2 v_tex_coords;

uniform sampler2D texture;

uniform vec2 screen_resolution;

uniform vec2 curvature = vec2(3.0, 3.0);
uniform vec2 scanline_opacity = vec2(0.5, 0.5);
uniform float brightness = 2.0;

out vec4 f_color;

const float PI = 3.14159;

vec2 curve_uv(vec2 uv) {
    uv = uv * 2.0 - 1.0;
    vec2 offset = abs(uv.yx) / vec2(curvature.x, curvature.y);
    uv = uv + uv * offset * offset;
    uv = uv * 0.5 + 0.5;
    return uv;
}

vec3 scanline_intensity(float uv, float resolution, float opacity) {
    float intensity = sin(uv * resolution * PI * 2.0);
    intensity = ((0.5 * intensity) + 0.5) * 0.9 + 0.1;
    return vec3(pow(intensity, opacity));
}

void main() {
    vec2 curved_uv = curve_uv(v_tex_coords);
    vec3 base_color = texture2D(texture, curved_uv).rgb;

    base_color += vec3(1.0/256.0);
    base_color *= scanline_intensity(curved_uv.x, screen_resolution.y, scanline_opacity.x);
    base_color *= scanline_intensity(curved_uv.y, screen_resolution.x, scanline_opacity.y);
    base_color *= vec3(brightness);

    if (curved_uv.x < 0.0 || curved_uv.y < 0.0 || curved_uv.x > 1.0 || curved_uv.y > 1.0) {
        f_color = vec4(0.0, 0.0, 0.0, 1.0);
    } else {
        f_color = vec4(base_color, 1.0);
    }
}
