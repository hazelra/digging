#version 330 core

uniform mat3 matrix;

in vec2 position;
in vec2 tex_coords;

out lowp vec2 v_tex_coords;

void main() {
    gl_Position = vec4(matrix * vec3(position, 1.0), 1.0);

    v_tex_coords = tex_coords;
}
