#version 330 core
precision lowp float;

in vec2 v_tex_coords;

uniform sampler2D texture;

out vec4 f_color;

void main() {
    f_color = vec4(texture2D(texture, v_tex_coords).rgb, 1.0);
}
